# Accessing the app

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script> -->

If you are a part of a group, you need to log in to see your group's fields. If you are not a part of a group yet, ask the group admin to create an account for you.


### Accepting SurveyStack invitations
Invitations are sent out by group administrators, and some groups may use *white-labelled* apps. So the invitation may take you to a SoilStack labelled app or to an app with branding specific to the group (similar to the example below).

1. You will receive an email from *teravestdan@gmail.com* with subject line “SoilStack Invite - `[Your Group]`. Click the link in the email to activate your invitation.

2\. The link will take you to the invitation page.
     - If you already created a SoilStack account, select `LOGIN` and sign in using your existing username and password.
     - If you do not already have a SoilStack account, select `create account` and fill in your information to sign up.

3\. After logging in or creating your account, click `join [Your Group]` and you will be taken to that group's home screen and are able to access your fields.

### Installing SoilStack using Chrome

1. Go to app.soilstack.io, and select “Add SoilStack to Homescreen” from bottom 
2. If the option is not available, select the menu from the top right and select “Install App” 
3. Select “Install”
4. SoilStack will now be installed on your device and will work offline 

### FirefoxInstalling SoilStack using Firefox 

1. Go to app.soilstack.io, and  select the menu from the  bottom right of the page 
2. Select “Install” 
3. Select “Add” 
4. SoilStack will now be installed on your device and will work offline 

### SafariInstalling SoilStack using Safari 

1. Go to app.soilstack.io, and  select the options button on  the bottom of the page 
2. Select “Add to Home  Screen” 
3. Select “Add”
4. SoilStack will now be installed on your device and will work offline 






### Sidebar menu

COLLECT  
- **My Submissions** - once you have started a survey, it will be saved in the `My Submissions` tab. If the survey was both completed and submitted it will be saved in the `SENT` tab, otherwise it will be saved in the `Drafts` tab  
- **Browse** surveys that are in your group, that you have created, or all the surveys available on SurveyStack

ADMIN  
- **Builder** - build your own surveys.  
- **Scripts** - build your own scripts.  
- **Groups** - create your own group. 

### User profile
View and edit your profile by clicking on your email address in the upper right corner of the page.

- **Profile**  
- **Edit Account**
- **Select Active Group**
- **Sign Out**

### Accepting SurveyStack invitations
Invitations are sent out by group administrators, and some groups may use *white-labelled* apps. So the invitation may take you to a SurveyStack labelled app or to an app with branding specific to the group (like in the *Real Food Campign* example below).

1. You will receive an email from *invitation@surveystack.io* with subject line “Surveystack invitation to group `[Your Group]`. Click the link in the email to activate your invitation.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/invitation_email.png)

2\. The link will take you to the invitation page.
     - If you already created a SurveyStack account, select `LOGIN` and sign in using your existing username and password.
     - If you do not already have a SurveyStack account, select `create account` and fill in your information to sign up.

3\. After logging in or creating your account, click `join [Your Group]` and you will be taken to that group's home screen.

![image](https://gitlab.com/our-sci/software/surveystack_tutorials/-/raw/master/docs/img/join_group.png)

