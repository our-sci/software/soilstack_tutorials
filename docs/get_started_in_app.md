# Get Started in the App
## Welcome to SoilStack 
SoilStack is a flexible Progressive Web App that allows you to do soil sampling related things.

SoilStack is a progressive web app, not a native app, which  means you do not go to the apple app store or google  playstore to download the app. Instead, you go to  app.soilstack.io and Install or Add to Homescreen depending on your device and browser type. 


| Term | Definition |
| ---  | ---        |
| Sampling collection | Collecting soil samples from all the sampling locations for the entire field. |
| Sampling location | Every location in that field where you put the soil probe in the ground. |
| Sampling | Collecting all of the soil samples from that sampling location <br> (could be 1 or more samples). |
| Sample | individual soil sample (ex: if you are collecting multiple depth <br> increments at a sampling location each depth increment is a sample)|

## Navigating the App