# Using the app

## Before Heading to the Field:

- Accept the “SoilStack Invitation”
- “Install” the app to the Homescreen.
- QR codes generated and printed
    - QR Codes do not need to be producer specific or link to specific locations in the field. They only need to be unique within the project.
    - If you are collecting regular soil cores and separate bulk density cores, you can create a separate set of QR codes with a ‘bd’ prefix or suffix to distinguish between the sample types.
- Set field to “offline” to give access to fields even with poor connectivity  

### Improving Offline Functionality

- If you anticipate not having internet access in the field, you can improve the apps performance by selecting “offline” in advance.
    - You must be connected to the internet when you switch to offline
    - This will download the base layers so you can navigate better in the field
    - The app will still function offline, even if the "offline" toggle is off, but the map layers may not be visible

## Searching for Fields - Desktop

Need to add the the images to the this doc

## Getting to the Field

1. Select a field

2. Under "Address" click either:
    - "Navigate to Contact Address" to navigate to the contact address on file
    - "Navigate to Field Location" to navigate to the centroid of the field

## Collecting Soil Samples

1. Select a field
2. Push the "COLLECT" button Under the map of the field 
3. Enter the minimum and maximum depth for the 1st depth increment you are collecting.
4. Select “Add depth” and enter the min and max depths for the next depth increment.
    - 0-30 cm
    - 0-15 cm, 15-30 cm, etc
5. Select “start” to begin sampling collection.

`The depth ranges you set here will be available for each sample in the dropdown menu for the entirety of the sampling collection!!`

## Using GPS to Navigate the Field

1. Click the icon in the upper right corner of the map to active your location
2. “Allow” the app to use your device’s location
3. Select a point to navigate to
    - When moving, the app will tell you how far away you are from the selected point and how accurate the device’s location is

### Troubleshooting Navigation Issues

- I don’t see my location on the map

    - If you cannot see your location on the map, and the icon in the upper right is orange, it means that you are not within the frame of the map. Once you move closer to a sample point your location should appear.

## Arriving at a sample location

1. When you get to a collection location select “Collect Sampling”
2. The “Min Depth” and “Max Depth” show the full range of depths entered when setting up your plan. 
    - You can ignore these here. Or you can change the depth if you are unable to reach the depth with your sample????
3. Select the “+” icon to add a sample.
`The timestamp and GPS location are captured when you select “COLLECT SAMPLING, but are not recorded until you press “SAVE”`

## Capturing Bag ID QR Code

1. Select the QR code button 
2. Allow the app to use your camera
3. Scan the QR code on your sample bag

## Selecting Depth of sample

1. Select the depth from the dropdown menu
2. Select "Add"
3. By selecting Custom you can enter in any depth increment

## Updating GPS Location When Changing Location

What if I cannot sample at the location?
The ESMC protocol allows soil samplers to move up to 10 meters away from the sampling location if they cannot collect a sample for some reason. (Ex: rocky or compacted layer, in/near standing water, in driving lane in the field, etc)

The App captures the GPS location when you select COLLECT SAMPLING. If you need to update the location to a new sampling location:
1. Press the target icon next to the GPS location to manually update the location OR
2. CANCEL out  of the sampling collection page and re-enter the page

## Check Progress

1. Select the progress menu in the upper left corner
2. See each sample collected at each sampling location
3. Your progress is automatically saved, so you can leave and come back!

## Editing Samples

1. Select the sampling location that you want to edit. 
2. Choose to Edit or Delete
    - Deleting the sampling data will reset that location so you can re-sample
3. Select the sample you want to edit, you can then edit the label or depth of the sample

## Returning to in progress Sampling Collections

### From the Fields Page

Once you have selected your sampling depths you have created a Sampling Collection

If you select Collect from the fields page and a Sampling Collection exists, you can go directly to that “in progress” collection or start a new one.

### From the Sampling Collection Page

To return to or edit an existing Sampling Collection, select the sampling collection and then select EDIT.

## Transferring a Sampling Collection to a different device

You can EXPORT and Sampling Collection and IMPORT it onto a different device

On the device with the Sampling Collection:
1. Navigate to Sampling Collections
2. click on the field in progress 
3. click edit
4. click on the number of samples in the upper left corner.
5. click export
6. Select "RAW JSON" and file type
need to add more info for this

## Checklists

### Before Heading to the Field:
- Accept the “SoilStack Invitation”
- “Install” the app to the Homescreen.
- QR codes generated and printed
- Set field to “offline” is you anticipate poor connectivity

### After Sampling:
- Check that all points are sampled
- Submit the "Sampling Collection" 

## Submitting a Sampling Collection

- Walk through how to submit

### Data submitted with a Sampling Collection

- Label ID
- Sample ID
- Sampling ID (Location)
- Sampling Collection ID (Field + time)
- Depth range of sample
- Actual GPS waypoints
- Accuracy of GPS at time of collection
- Intended GPS location of sampling
- Timestamp 

## Exporting a Lab Inventory

Once you have Submitted a sampling collection, you can select “LAB INVENTORY”.

Or, from the Sampling Collections Tab you can check multiple Sampling Collections and then select “LAB INVENTORY”

This will download a csv of the sample inventory to your device

`Email the downloaded lab inventory to the lab that is analyzing the samples`

## What’s in the Lab Inventory

Lab inventory includes:
- Label ID
- Sample ID
- Sampling ID (Location)
- Sampling Collection ID (Field + time)
- Depth range of sample

If the lab returns results that keep the “Sample ID”, “Sampling ID” and “Sampling Collection ID” we can automate merging of lab results with sampling metadata 

## Creating QR codes (example)

### Creating label ID’s in Excel:

Create a simple alphanumeric QR code for each sample. For example, let’s say you determine John Smith’s field has 40 sampling locations. First, determine how many total samples you are collecting at those 40 sampling locations. If you are collecting only 1 soil sample (ex: 0-30 cm only) then you only need 40 labels, if you are collecting 2 samples at each location (ex: 0-15 cm and 15-30 cm depth samples) then you need 80 labels.  

1. A simple, human readable coding structure is to use the first 2-3 letters of the farmers last name and then first name. In the example below, I use ‘smjo’ for John Smith.   

2. In a spreadsheet enter “smjo” in column A and copy/fill the column with as many labels as needed for John Smith.

3. In column B, use the “Fill” feature to add sequential numbers for each sample.

4. In column C, use the concatenate feature to create the final sample ID (ex: smjo12).

5. Copy column C using “Values only” and paste that column into column D, then delete columns A-C. 

6. Save file. 

### Create labels. Go to avery.com and create an account (its free)

1. Select “start a new project” or upload the attached template. If you upload the attached template, skip to step ADD HERE

2. Click on template #5160 and select “Select this Template.” This is the standard mailing address label and is widely available.

3. Select the basic empty design for the template.

4. From the left side menu click on “More” and then “QR and Barcodes” 

5. Select Spreadsheet or Sequential number 

6. Select Browse for File and use the QR code standard format

7. Select edit all on the right side of the screen, make the QR code the correct size, and position it
    - If you are sampling multiple depths, you can use the “Text” tab to enter depth increments and checkboxes. 

8. Go to import Data Mail Merge 

9. Add in the text you want to display and arrange on the label

10. Print your labels
